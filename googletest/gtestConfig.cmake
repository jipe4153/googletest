include("${CMAKE_CURRENT_LIST_DIR}/gtestTargets.cmake")

set(ROOT_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../)
include_directories("${ROOT_DIR}/include/gtest")
include_directories("${ROOT_DIR}/include/")
link_directories("${ROOT_DIR}/lib/")